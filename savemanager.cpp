#include "savemanager.h"

SaveManager::SaveManager()
{

}

bool SaveManager::Save(std::vector<GlobalShip *> profile, int credits, int gold, unsigned long score)
{


    std :: ofstream saveFile;
    saveFile.open ("save.txt", std :: ofstream :: trunc);
    if (saveFile.is_open())
    {
        for (int i = 0; i < 3; i++) // 6 * 3 + 3
        {

            saveFile << profile.at(i)->currentHpUpgrade << std:: endl;
            saveFile << profile.at(i)->currentSpeedUpgrade << std:: endl;
            saveFile << profile.at(i)->currentGunCountUpgrade << std:: endl;
            saveFile << profile.at(i)->currentGunSpreadUpgrade << std:: endl;
            saveFile << profile.at(i)->currentShootingRateUpgrade << std:: endl;
            saveFile << profile.at(i)->extraLife << std:: endl;
            saveFile << profile.at(i)->bought << std :: endl;

        }
        saveFile << credits << std:: endl;
        saveFile << gold << std:: endl;
        saveFile << score << std:: endl;
        saveFile.close();
        return true;
    }
    return false;

}

bool SaveManager::Load(std::vector<GlobalShip *> &profile, int &credits, int &gold, unsigned long &score)
{
    std :: ifstream saveFile;
    std :: string line;
    saveFile.open ("save.txt");
    if (saveFile.is_open())
    {
        for (int i = 0; i < 3; i++) // 6 * 3 + 3
        {
            saveFile >> profile.at(i)->currentHpUpgrade;
            saveFile >> profile.at(i)->currentSpeedUpgrade;
            saveFile >> profile.at(i)->currentGunCountUpgrade;
            saveFile >> profile.at(i)->currentGunSpreadUpgrade;
            saveFile >> profile.at(i)->currentShootingRateUpgrade;
            saveFile >> profile.at(i)->extraLife;
            saveFile >> profile.at(i)->bought;

        }
        saveFile >> credits;
        saveFile >> gold;
        saveFile >> score;
        saveFile.close();
        return true;
    }
    return false;
}
