#include "mainwindow.h"

Graphics::GameRenderer::GameRenderer(QWidget *parent):
    QGLWidget(parent)
{


#ifndef ANDROID
    setGeometry(QRect(50, 50, Window.width(), Window.height()));
    setFixedSize(Window);

    QGLFormat format;
    format.setSamples(16);
    setFormat(format);
#else
    QScreen *screen = QGuiApplication::primaryScreen();
    ScreenScale = std::min(screen->geometry().width() / Window.width(), screen->geometry().height() / Window.height());
#endif

    UpgradeLabels[UpgradeType::Health] = "HEALTH";
    UpgradeLabels[UpgradeType::WeaponNumber] = "BARRELS";
    UpgradeLabels[UpgradeType::FireRate] = "FIRE RATE";
    UpgradeLabels[UpgradeType::Accuracity] = "ACCURACITY";
    UpgradeLabels[UpgradeType::Speed] = "SPEED";

    MenuData.resize(4);
    {
        int width = 400;
        int height = 100;
        int offset = 30;
        int y = (Window.height() - height * 2 - offset) / 2 - Window.height() / 4;
        int x = (Window.width() - width) / 2;
        MenuData[0].resize(2);
        MenuData[0][0] = QRect(x, y, width, height);
        MenuData[0][1] = QRect(x, y + height + offset, width, height);
    }
    {
        MenuData[1].resize((int)UpgradeType::Count + 3 + 2);
        int width = 400;
        int height = 75;
        int offset = 20;
        int y = (Window.height() - (height + offset) * ((int)UpgradeType::Count + 2) + offset) / 2;
        int x = (Window.width() - width) / 2;

        int shipWidth = (width - offset * 2) / 3;
        for (int i = 0; i < 3; i++) {
            MenuData[1][(int)UpgradeType::Count + i] = QRect(x + (shipWidth + offset) * i, y, shipWidth, height);
        }
        y += height + offset;

        for (int i = 0; i < (int)UpgradeType::Count; i++) {
            MenuData[1][i] = QRect(x, y + (height + offset) * i, width, height);
        }
        y += (height + offset) * (int)UpgradeType::Count;

        MenuData[1][MenuData[1].size() - 2] =  QRect(x + height + offset, y, width - height - offset, height);
        MenuData[1][MenuData[1].size() - 1] =  QRect(x, y, height, height);
    }
    {
        MenuData[3].resize(2);

        int width = 200;
        int height = 100;
        int offset = 20;
        int x = (Window.width() - (width + offset) * 2 + offset) / 2;
        int y = (Window.height() - height) / 2;
        MenuData[3][0] = QRect(x, y, width, height);
        MenuData[3][1] = QRect(x + width + offset, y, width, height);
    }

    MenuPen.setWidth(2);
    MenuPen.setColor(Qt::white);
    MenuPen.setJoinStyle(Qt::MiterJoin);

    for (int i = 0; i < 4; i++) {
        ShipData.push_back(QString(":/data/ship_%1.png").arg(i));
    }

    for (int i = 0; i < 2; i++) {
        ProjectileData.push_back(QPixmap(QString(":/data/projectile_%1.png").arg(i)));
    }

    HealthData.first = QPixmap(":/data/health.png");
    HealthData.second = QPixmap(":/data/health_empty.png");

    ExplosionData = QPixmap(":/data/explosion_30.png");
    ExplosonFrames = ExplosionData.width() / ExplosionData.height();
    ExplosionDuration = 30;

    BackgroundStarList.resize(50);
    for (BgStar &star: BackgroundStarList) {
        GenerateBgStar(star, true);
    }
    RenderTimerID = startTimer(1000 / 60.0);
    FpsTimerId = startTimer(1000);
}

Graphics::GameRenderer::~GameRenderer()
{

}

void Graphics::GameRenderer::AddShip(const IShip *ship) {
    ShipList.insert(ship);
}

void Graphics::GameRenderer::AddProjectile(const IProjectile *projectile) {
    ProjectileList.insert(projectile);
}

void Graphics::GameRenderer::RemoveShip(const IShip *ship) {
    ShipList.erase(ship);
}

void Graphics::GameRenderer::DestroyShip(const IShip *ship) {
    ExplosionList.push_back(std::make_pair(ship->GetPosition(), 0));
    ShipList.erase(ship);
}

void Graphics::GameRenderer::RemoveProjectile(const IProjectile *projectile) {
    ProjectileList.erase(projectile);
}

void Graphics::GameRenderer::ClearBattle() {
    ShipList.clear();
    ProjectileList.clear();
}

void Graphics::GameRenderer::SetGame(IGame *game) {
    Game = game;
}

void Graphics::GameRenderer::GameOver() {
    SetMenuState(MenuType::GameOver);
}

void Graphics::GameRenderer::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.scale(ScreenScale, ScreenScale);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    DrawBackground(painter);

    painter.setPen(MenuPen);

    if (State == MenuType::Battle || State== MenuType::GameOver) {
        DrawShips(painter);
        DrawProjectiles(painter);
        DrawExplosions(painter);
    }

    switch (State) {
        case MenuType::MainMenu:
            DrawMainMenu(painter);
            break;
        case MenuType::UpgradeMenu:
            DrawUpgradeMenu(painter);
            break;
        case MenuType::Battle:
            DrawBattleMenu(painter);
            break;
        case MenuType::GameOver:
            DrawGameOverMenu(painter);
            break;
    }

    painter.setPen(MenuPen);
    for (const QRect &rect: MenuData[(int)State]) {
        painter.drawRect(rect);
    }

#ifdef QT_DEBUG
    painter.setFont(QFont());
    painter.setPen(Qt::red);
    painter.drawText(10, 75, 200, 200, Qt::AlignTop | Qt::AlignLeft, QString("FPS: %1\nOBJECTS: %2").arg(FpsValue).arg(ShipList.size() + ProjectileList.size()));
#endif
}

void Graphics::GameRenderer::mousePressEvent(QMouseEvent *event) {
    if (State == MenuType::Battle) {
        BattleMenuPressEvent(event->pos());
        return;
    }

    QPoint position;
    int index = -1;
    for (int i = 0; i < MenuData[(int)State].size(); i++) {
        const QRect &rect = MenuData[(int)State][i];
        if (event->pos().x() > rect.left() && event->pos().x() < rect.right() &&
            event->pos().y() > rect.top() && event->pos().y() < rect.bottom())
        {
            index = i;
            position = event->pos() - rect.topLeft();
            break;
        }
    }
    if (index == -1) return;

    switch (State) {
        case MenuType::MainMenu:
            MainMenuPressEvent(index, position);
            break;
        case MenuType::UpgradeMenu:
            UpgradeMenuPressEvent(index, position);
            break;
        case MenuType::GameOver:
            GameOverMenuPressEvent(index, position);
            break;
    }
}

void Graphics::GameRenderer::mouseReleaseEvent(QMouseEvent *event) {
    if (State == MenuType::Battle) {
        Game->StopMoveToPoint();
    }
}

void Graphics::GameRenderer::mouseMoveEvent(QMouseEvent *event) {
    if (!(event->buttons() & Qt::LeftButton)) return;
    Game->MoveToPoint(std::make_pair(event->x(), event->y()));
}

void Graphics::GameRenderer::keyPressEvent(QKeyEvent *event) {
    UpdateControls(event->key(), true);
}

void Graphics::GameRenderer::keyReleaseEvent(QKeyEvent *event) {
    UpdateControls(event->key(), false);
}

void Graphics::GameRenderer::timerEvent(QTimerEvent *event) {
    if (event->timerId() == RenderTimerID) {
        if (State == MenuType::Battle) {
            Game->Update();
        }
        update();
        FpsCounter++;
    } else if (event->timerId() == FpsTimerId) {
        FpsValue = FpsCounter;
        FpsCounter = 0;
    }
}

void Graphics::GameRenderer::UpdateControls(int key, bool pressed) {
    KeyMap[key] = pressed;

    if (State == MenuType::Battle) {
        if (KeyMap[Qt::Key_Left]) Game->SetHorizontalMove(-1);
        else if (KeyMap[Qt::Key_Right]) Game->SetHorizontalMove(1);
        else Game->SetHorizontalMove(0);

        if (KeyMap[Qt::Key_Up]) Game->SetVerticalMove(-1);
        else if (KeyMap[Qt::Key_Down]) Game->SetVerticalMove(1);
        else Game->SetVerticalMove(0);
    }
}

void Graphics::GameRenderer::MainMenuPressEvent(int index, const QPoint &pos) {
    if (index == 0) {
        SetMenuState(MenuType::UpgradeMenu);
    } else if (index == 1) {
        qApp->exit();
    }
}

void Graphics::GameRenderer::UpgradeMenuPressEvent(int index, const QPoint &pos) {
    if (index < (int)UpgradeType::Count) {
        QSize rect = MenuData[1][0].size();
        if (Game->GetCurrentUpgradeLevel((UpgradeType)index) >= Game->GetMaxUpgradeLevel((UpgradeType)index)) return;
        if (pos.x() < rect.height()) Game->UpgradeCurrentShip((UpgradeType)index, true);
        if (pos.x() > rect.width() - rect.height()) Game->UpgradeCurrentShip((UpgradeType)index, false);
    } else if (index < (int)UpgradeType::Count + 3) {
        int shipId = index - (int)UpgradeType::Count;
        Game->SelectShip(shipId);
    } else if (index == MenuData[1].size() - 2) {
        Game->StartBattle();
        SetMenuState(MenuType::Battle);
    } else if (index == MenuData[1].size() - 1) {
        SetMenuState(MenuType::MainMenu);
    }
}

void Graphics::GameRenderer::BattleMenuPressEvent(const QPoint &pos) {
    if (pos.x() > Window.width() - 35 && pos.y() < 35) {
        SetMenuState(MenuType::UpgradeMenu);
        Game->StopBattle();
    } else {
        Game->MoveToPoint(std::make_pair(pos.x(), pos.y()));
    }
}

void Graphics::GameRenderer::GameOverMenuPressEvent(int index, const QPoint &pos) {
    if (index == 0) {
        Game->StopBattle();
        SetMenuState(MenuType::UpgradeMenu);
    } else {
        if (Game->Revive()) {
            SetMenuState(MenuType::Battle);
        }
    }
}

void Graphics::GameRenderer::DrawBackground(QPainter &painter) {
    QColor background = QColor::fromHsvF(BgColor, 1, 0.7 - 0.6 * BgColorProgress);
    painter.fillRect(0, 0, Window.width(), Window.height(), background);
    for (BgStar &star: BackgroundStarList) {
        if (star.Position.y() > Window.height()) GenerateBgStar(star, false);
        painter.setPen(star.Color);
        float velocity = star.Velocity * (0.1 + 0.9 * BgBoostProgress);
        painter.drawLine(star.Position, star.Position + QPointF(0, velocity));
        star.Position.ry() += velocity;
    }

    switch (BgState) {
        case BackgoundState::Menu:
            BgColor = 200.0 / 360.0;
            BgBoostProgress = std::max(0.0, BgBoostProgress - 0.01);
            BgColorProgress = std::min(1.0, BgColorProgress + 0.01);
            break;
        case BackgoundState::StartingFight:
            BgColor = 200.0 / 360.0;
            BgBoostProgress = std::min(1.0, BgBoostProgress + 0.01);
            if (BgBoostProgress >= 1) {
                if (BgColorProgress > 0) BgColorProgress = std::max(0.0, BgColorProgress - 0.01);
                else BgState = BackgoundState::Fight;
            }
            break;
        case BackgoundState::Fight:
            BgColorProgress = std::min(1.0, BgColorProgress + 0.0005);
            if (BgColorProgress >= 1){
                BgColor += 0.00025;
                if (BgColor > 1) BgColor -=1;
            }
            break;
    }

}

void Graphics::GameRenderer::DrawShips(QPainter &painter) {
    for (const IShip *ship: ShipList) {
        const QPixmap &pixmap = ShipData[ship->GetType()];
        auto pos = ship->GetPosition();
        painter.save();
        painter.translate(pos.first, pos.second);
        painter.rotate(ship->GetAngle() * 180 / M_PI);
        painter.drawPixmap(-pixmap.width() / 2, -pixmap.height() / 2, pixmap);
        painter.restore();
    }
}

void Graphics::GameRenderer::DrawProjectiles(QPainter &painter) {
    for (const IProjectile *proj: ProjectileList) {
        const QPixmap &pixmap = ProjectileData[proj->GetType()];
        auto pos = proj->GetPosition();
        painter.drawPixmap(pos.first - pixmap.width() / 2, pos.second - pixmap.height() / 2, pixmap);
    }
}

void Graphics::GameRenderer::DrawExplosions(QPainter &painter) {
    int frameDuration = ExplosionDuration / ExplosonFrames;
    for (auto it = ExplosionList.begin(); it != ExplosionList.end();) {
        it->second++;
        if (it->second >= ExplosionDuration) it = ExplosionList.erase(it);
        else {
            painter.drawPixmap(it->first.first - ExplosionData.height() / 2,
                               it->first.second - ExplosionData.height() / 2,
                               ExplosionData.height(), ExplosionData.height(), ExplosionData,
                               ExplosionData.height() * (it->second / frameDuration), 0,
                               ExplosionData.height(), ExplosionData.height());
            it++;
        }
    }
}

void Graphics::GameRenderer::DrawMainMenu(QPainter &painter) {
    QFont font;
    font.setBold(true);
    font.setPixelSize(MenuData[0][0].height() * 0.8);
    painter.setFont(font);
    painter.drawText(MenuData[0][0], Qt::AlignCenter, "PLAY");
    painter.drawText(MenuData[0][1], Qt::AlignCenter, "QUIT");
}

void Graphics::GameRenderer::DrawUpgradeMenu(QPainter &painter) {
    int pWidth = 10;
    int pHeight = 20;
    int pOffset = 10;

    QFont font;
    font.setBold(true);
    font.setPixelSize(MenuData[1][0].height() * 0.4);
    painter.setFont(font);
    for (int i = 0; i < (int)UpgradeType::Count; i++) {
        const QRect &rect = MenuData[1][i];
        painter.drawText(rect, Qt::AlignTop | Qt::AlignHCenter, UpgradeLabels[(UpgradeType)i].data());

        int maxLevel = Game->GetMaxUpgradeLevel((UpgradeType)i);
        int level = Game->GetCurrentUpgradeLevel((UpgradeType)i);
        int y = rect.bottom() - pHeight - pOffset;
        int x = rect.left() + (rect.width() - (pWidth + pOffset) * maxLevel + pOffset) / 2;
        for (int j = 0; j < maxLevel; j++) {
            QRect upgradeRect(x + (pWidth + pOffset) * j, y, pWidth, pHeight);
            if (j < level) painter.fillRect(upgradeRect, MenuPen.color());
            else painter.drawRect(upgradeRect);
        }
    }
    font.setPixelSize(MenuData[1][0].height() * 0.6);
    painter.setFont(font);
    for (int i = 0; i < (int)UpgradeType::Count; i++) {
        if (Game->GetCurrentUpgradeLevel((UpgradeType)i) == Game->GetMaxUpgradeLevel((UpgradeType)i)) continue;
        const QRect &rect = MenuData[1][i];
        painter.drawLine(rect.x() + rect.height(), rect.y(), rect.x() + rect.height(), rect.bottom());
        painter.drawLine(rect.right() - rect.height(), rect.y(), rect.right() - rect.height(), rect.bottom());
        painter.drawText(rect.x(), rect.y(), rect.height(), rect.height(), Qt::AlignHCenter | Qt::AlignTop, "🡅");
        painter.drawText(rect.right() - rect.height(), rect.y(), rect.height(), rect.height(), Qt::AlignHCenter | Qt::AlignTop, "🡅");
    }

    font.setPixelSize(MenuData[1][0].height() * 0.25);
    painter.setFont(font);

    for (int i = 0; i < (int)UpgradeType::Count; i++) {
        if (Game->GetCurrentUpgradeLevel((UpgradeType)i) == Game->GetMaxUpgradeLevel((UpgradeType)i)) continue;
        const QRect &rect = MenuData[1][i];
        auto cost = Game->GetUpgradeCost((UpgradeType)i);
        painter.drawText(rect.x(), rect.y(), rect.height(), rect.height() - 5, Qt::AlignHCenter | Qt::AlignBottom, CreditSymbol + QString::number(cost.first));
        painter.drawText(rect.right() - rect.height(), rect.y(), rect.height(), rect.height() - 5, Qt::AlignHCenter | Qt::AlignBottom, GoldSymbol + QString::number(cost.second));
    }

    font.setPixelSize(MenuData[1][(int)UpgradeType::Count].height() * 0.25);
    painter.setFont(font);

    QTransform shipTransform;
    shipTransform.rotate(-90);

    for (int i = 0; i < 3; i++) {
        const QRect &rect = MenuData[1][(int)UpgradeType::Count + i];

        if (Game->IsShipUnlocked(i)) {
            if (i == Game->GetSelectedShip()) {
                painter.fillRect(rect, MenuPen.color());
                painter.setPen(Qt::black);
            } else {
                painter.setPen(Qt::white);
            }
        } else {
            auto price = Game->GetShipPrice(i);
            painter.setPen(MenuPen);
            painter.drawText(rect.x(), rect.y(), rect.width(), rect.height() - 5, Qt::AlignHCenter | Qt::AlignBottom,
                             (price.second ? GoldSymbol : CreditSymbol) + QString::number(price.first));

            painter.setPen(Qt::white);
        }

        QPixmap ship = ShipData[i].transformed(shipTransform);
        float scale = std::min(rect.width() / (float)ship.width(), rect.height() * 0.6f / (float)ship.height());
        QSize scaledSize(scale * ship.width(), scale * ship.height());
        painter.drawPixmap(rect.x() + (rect.width() - scaledSize.width()) / 2,
                           rect.y() + 5, scaledSize.width(), scaledSize.height(), ship.mask());
    }
    painter.setPen(MenuPen);

    font.setPixelSize(MenuData[1].back().height() * 0.8);
    painter.setFont(font);
    painter.drawText(MenuData[1][MenuData[1].size() - 2], Qt::AlignCenter, "FIGHT");
    painter.drawText(MenuData[1].back(), Qt::AlignCenter, "🡄");

    int labelsWidth = 40;
    font.setPixelSize(labelsWidth * 0.8);
    painter.setFont(font);

    auto money = Game->GetMoney();
    painter.drawText(0, 20, Window.width(), labelsWidth, Qt::AlignCenter,
                     CreditSymbol + QString::number(money.first) + QString(4, ' ')
                     + GoldSymbol +QString::number(money.second));
    painter.drawText(0, Window.height() - 25 - labelsWidth, Window.width(), labelsWidth, Qt::AlignCenter,
                     QString("MAX SCORE: %1").arg(Game->GetMaxScore()));
}

void Graphics::GameRenderer::DrawBattleMenu(QPainter &painter) {
    int maxHealth = Game->GetMaxHealth();
    int health = Game->GetCurrentHealth();
    int offset = 10;
    int x = 25;
    for (int i = 0; i < maxHealth; i++) {
        const QPixmap *icon;
        if (i < health) icon = &HealthData.first;
        else icon = &HealthData.second;
        painter.drawPixmap(x, 25, *icon);
        x += icon->width() + offset;
    }
    QFont font;
    font.setBold(true);
    font.setPixelSize(25);
    painter.setFont(font);
    painter.drawText(0, 10, Window.width(), 100, Qt::AlignHCenter | Qt::AlignTop, QString::number(Game->GetScore()));
    painter.drawText(0, 10, Window.width() - 10, 100, Qt::AlignRight | Qt::AlignTop, "❌");
}

void Graphics::GameRenderer::DrawGameOverMenu(QPainter &painter) {
    QFont font;
    font.setBold(true);
    font.setPixelSize(MenuData[3][0].height() * 0.8);
    painter.setFont(font);
    painter.drawText(MenuData[3][0], Qt::AlignCenter, "🡄");

    font.setPixelSize(MenuData[3][1].height() * 0.5);
    painter.setFont(font);

    int reviveCost = Game->GetReviveCost();
    if (reviveCost) {
        painter.drawText(MenuData[3][1], Qt::AlignHCenter | Qt::AlignTop, "REVIVE");

        font.setPixelSize(MenuData[3][1].height() * 0.4);
        painter.setFont(font);
        painter.drawText(MenuData[3][1], Qt::AlignHCenter | Qt::AlignBottom, GoldSymbol + QString::number(reviveCost));
    } else {
        painter.drawText(MenuData[3][1], Qt::AlignCenter, "REVIVE");
    }

    font.setPixelSize(Window.height() * 0.1);
    painter.setFont(font);
    painter.drawText(0, 0, Window.width(), Window.height() / 4, Qt::AlignHCenter | Qt::AlignBottom, "GAME OVER");

    auto money = Game->GetMoney();
    font.setPixelSize(Window.height() * 0.04);
    painter.setFont(font);
    QString scoreText = QString("SCORE: %1").arg(Game->GetScore());

    QFontMetrics metrics(font);
    int width = metrics.width(scoreText);
    painter.drawText((Window.width() - width) / 2, Window.height() / 4, Window.width() * 2 / 3, Window.height() / 4, Qt::AlignLeft | Qt::AlignTop,
                     QString("%1\n%2%3 + %4\n%5%6").arg(scoreText).arg(CreditSymbol).arg(money.first)
                     .arg(Game->GetEarnedCredits()).arg(GoldSymbol).arg(money.second));
}

void Graphics::GameRenderer::GenerateBgStar(BgStar &star, bool anywhere) {
    star.Color = Qt::white;
    float travelTime = (1 + rand() % 3) * 60;
    star.Velocity = Window.height() / travelTime;
    star.Position.rx() = rand() % Window.width();
    star.Position.ry() = anywhere ? rand() % Window.height() : 0;
}

void Graphics::GameRenderer::SetMenuState(Graphics::GameRenderer::MenuType state) {
    switch (state) {
        case MenuType::MainMenu:
            BgState = BackgoundState::Menu;
            break;
        case MenuType::UpgradeMenu:
            BgState = BackgoundState::Menu;
            break;
        case MenuType::Battle:
            Game->StopMoveToPoint();
            BgState = BackgoundState::StartingFight;
            break;
        case MenuType::GameOver:
            break;
    }
    State = state;
}
