#ifndef SAVEMANAGER_H
#define SAVEMANAGER_H
#include <vector>
#include "objects.h"
#include <iostream>
#include <fstream>
#include <tuple>


class SaveManager
{
public:
    SaveManager();
   static bool Save(std::vector<GlobalShip*> profile, int credits, int gold, unsigned long score);
   static bool Load(std::vector<GlobalShip*> &profile, int &credits, int &gold, unsigned long &score);
};

#endif // SAVEMANAGER_H
