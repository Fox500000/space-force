#include "game.h"
#include <algorithm>
#include "savemanager.h"

void Game::ClearField()
{
    for (auto i : enemies) {
         window.DestroyShip(i);
         delete i;
    }
    enemies.clear();
    for (auto i : playerProjectiles) {
         window.RemoveProjectile(i);
         delete i;
    }
    playerProjectiles.clear();
    for (auto i : enemyProjectiles) {
         window.RemoveProjectile(i);
         delete i;
    }
    enemyProjectiles.clear();
}

Game::Game(IRenderer& windowIn): window(windowIn)
{
     player = new Ship(0, make_pair(300, 750), 0, 0);
     playerUpgrades.push_back(new GlobalShip(0)); //made by Sanya
     playerUpgrades.push_back(new GlobalShip(1)); //made by Sanya
     playerUpgrades.push_back(new GlobalShip(2)); //made by Sanya
     SaveManager :: Load(playerUpgrades, credits, gold ,maxScore);
}

Game::~Game()
{
    SaveManager::Save(playerUpgrades, credits, gold, maxScore);
    ClearField();
    for(auto i : playerUpgrades) {
        delete i;
    }
}

int Game::GetSelectedShip() const
{
     return chosenType;
}

std::pair<int, int> Game::GetUpgradeCost(UpgradeType type) const
{
     return playerUpgrades.at(chosenType)->GetCost(type);

     //return std::make_pair(100, 5);
}

int Game::GetCurrentUpgradeLevel(UpgradeType type) const
{
     switch (type) {
          case UpgradeType :: Health:
               return playerUpgrades.at(chosenType)->currentHpUpgrade;

          case UpgradeType :: WeaponNumber:
               return playerUpgrades.at(chosenType)->currentGunCountUpgrade;

          case UpgradeType :: FireRate:
               return playerUpgrades.at(chosenType)->currentShootingRateUpgrade;

          case UpgradeType :: Accuracity:
               return playerUpgrades.at(chosenType)->currentGunSpreadUpgrade;

          case UpgradeType :: Speed:
               return playerUpgrades.at(chosenType)->currentSpeedUpgrade;

          default:
               throw std::invalid_argument("No such upgrade type");
     }
     //return type == UpgradeType::Health ? 5 : 1;
}

int Game::GetMaxUpgradeLevel(UpgradeType type) const
{
     switch (type) {
          case UpgradeType :: Health:
               return playerUpgrades.at(chosenType)->maxHpUpgrade;

          case UpgradeType :: WeaponNumber:
               return playerUpgrades.at(chosenType)->maxGunCountUpgrade;

          case UpgradeType :: FireRate:
               return playerUpgrades.at(chosenType)->maxShootingRateUpgrade;

          case UpgradeType :: Accuracity:
               return playerUpgrades.at(chosenType)->maxGunSpreadUpgrade;

          case UpgradeType :: Speed:
               return playerUpgrades.at(chosenType)->maxSpeedUpgrade;

          default:
               throw std::invalid_argument("No such upgrade type");
     }
     //return 5;
}

std::pair<int, int> Game::GetMoney() const
{
     return make_pair(credits, gold);
     // return std::make_pair(100, 5);
}

bool Game::IsShipUnlocked(int id) const
{
     return playerUpgrades.at(id)->bought;
}

std::pair<int, bool> Game::GetShipPrice(int id) const
{
     if (id != 2) {
          return make_pair(playerUpgrades.at(chosenType)->ShipPriceInCredits[id], false);
     }
     if (id == 2) {
          return make_pair(playerUpgrades.at(chosenType)->ShipPriceInCredits[id], true);
     }
     throw std::invalid_argument("Invalid index to get Ship Price");
}

int Game::GetMaxScore() const
{
     return maxScore;
}

int Game::GetReviveCost() const
{
    if (playerUpgrades.at(chosenType)->extraLife > 0) return 0;
    else return (revivesCount + 1) * 100;
}

int Game::GetMaxHealth() const
{
    return playerUpgrades.at(chosenType)->maxHp;
}

int Game::GetCurrentHealth() const
{
     return player->health;
}

int Game::GetScore() const
{
     return score;
}

int Game::GetEarnedCredits() const
{
     return earnedCredits;
}

bool Game::UpgradeCurrentShip(UpgradeType type, bool creditsSelected)
{

     return playerUpgrades.at(chosenType)->UpgradeShip(type, creditsSelected, credits, gold);
}

bool Game::SelectShip(int id)
{
    if (playerUpgrades.at(id)->bought)
    {

    chosenType = id;
     player->type = id;
     return true;

    }
    else
    {
        std::pair<int, bool> condition = GetShipPrice(id);
       if (condition.second)
       {
           if (gold - condition.first >= 0)
           {
               gold -= condition.first;
               chosenType = id;
                player->type = id;
                playerUpgrades.at(id)->bought = true;
                return true;
           }
           else
           {
               return false;
           }
       }
       else
       {
           if (credits - condition.first >= 0)
           {
               credits -= condition.first;
               chosenType = id;
                player->type = id;
                playerUpgrades.at(id)->bought = true;
                return true;
           }
           else
           {
               return false;
           }
       }
    }
     // playerUpgrades->Setup(id);

}

void Game::StartBattle()
{
    SaveManager :: Save(playerUpgrades, credits, gold, maxScore);
     playerUpgrades.at(chosenType)->UpdateUpgrades();
     if (chosenType == 2)
     {
         playerUpgrades.at(chosenType)->extraLife = 1;
     }
     earnedCredits = 0;
     score = 0;
     revivesCount = 0;
     player->Setup(playerUpgrades.at(chosenType));
     player->SetPosition(300, 750);
     window.AddShip(player);
}

void Game::StopBattle()
{
     window.ClearBattle();
     for (auto i : enemies) {
          delete i;
     }
     enemies.clear();
     for (auto i : playerProjectiles) {
          delete i;
     }
     playerProjectiles.clear();
     for (auto i : enemyProjectiles) {
          delete i;
     }
     enemyProjectiles.clear();
     credits += earnedCredits;
     if (score > maxScore) maxScore = score;
     window.RemoveShip(player);
}

bool Game::Revive()
{
    if (playerUpgrades.at(chosenType)->extraLife > 0)
    {
        ClearField();
        playerUpgrades.at(chosenType)->extraLife--;
        player->Setup(playerUpgrades.at(chosenType));
        window.AddShip(player);
        return true;
    }
    int reviveCost = GetReviveCost();
    if (gold - reviveCost >= 0)
    {
        ClearField();
        gold -= reviveCost;
        player->Setup(playerUpgrades.at(chosenType));
        window.AddShip(player);
        revivesCount++;
        return true;
    }
    return false;
}

void Game::Update()
{
     vector<int> deleteEnemies;
     vector<int> deleteProjectiles;
     deleteEnemies.clear();
     deleteProjectiles.clear();
     if (fpsCounter > std::max(40LL, 120 - (long long)score / 100)) {
          fpsCounter = 0;
          SpawnRandomPattern(*this, &window); //round(rand() % 4)
     }
     else {
          fpsCounter++;
     }

     for (int i = 0; i < enemies.size(); i++) {
          if (CheckCollision(player, enemies.at(i))) {
                    player->health -= 1;
                    if (!(std::find(deleteEnemies.begin(), deleteEnemies.end(), i) != deleteEnemies.end())) {
                        deleteEnemies.push_back(i);
                    }
                    earnedCredits += 10;
          }
          if (enemies.at(i)->GetPosition().second > windowY + 100) {
              if (!(std::find(deleteEnemies.begin(), deleteEnemies.end(), i) != deleteEnemies.end())) {
                  deleteEnemies.push_back(i);
              }
          }
     }

     for (int i = deleteEnemies.size() - 1; i > -1; i--) {
          window.DestroyShip(enemies.at(deleteEnemies.at(i)));
          delete enemies.at(deleteEnemies.at(i));
          enemies.erase(enemies.begin() + deleteEnemies.at(i));
     }
     deleteEnemies.clear();

     for (int i = 0; i < enemyProjectiles.size(); i++) {
          if (CheckCollision(player, enemyProjectiles.at(i))) {
               player->health--;
               if (!(std::find(deleteProjectiles.begin(), deleteProjectiles.end(), i) != deleteProjectiles.end())) {
                    deleteProjectiles.push_back(i);
               }
          }
          if (enemyProjectiles.at(i)->GetPosition().second > windowY + 100) {
              if (!(std::find(deleteProjectiles.begin(), deleteProjectiles.end(), i) != deleteProjectiles.end())) {
                   deleteProjectiles.push_back(i);
              }
          }
     }
     for (int i = deleteProjectiles.size() - 1; i > -1; i--) {
          window.RemoveProjectile(enemyProjectiles.at(deleteProjectiles.at(i)));
          delete enemyProjectiles.at(deleteProjectiles.at(i));
          enemyProjectiles.erase(enemyProjectiles.begin() + deleteProjectiles.at(i));
     }
     deleteProjectiles.clear();


     for (int i = 0; i < enemies.size(); i++) {
         for (int j = 0; j < playerProjectiles.size(); j++) {
             if (CheckCollision(enemies.at(i), playerProjectiles.at(j))) {
                 if (enemies.at(i)->health <= 1) {
                     //window.DestroyShip(enemies.at(i));
                     //enemies.erase(enemies.begin() + i);
                     //window.RemoveProjectile(playerProjectiles.at(j));
                     //playerProjectiles.erase(playerProjectiles.begin() + j);
                     if (!(std::find(deleteEnemies.begin(), deleteEnemies.end(), i) != deleteEnemies.end())) {
                         deleteEnemies.push_back(i);
                     }
                     if (!(std::find(deleteProjectiles.begin(), deleteProjectiles.end(), j) != deleteProjectiles.end())) {
                         deleteProjectiles.push_back(j);
                     }
                     earnedCredits += 10;
                 }
                 else {
                     //window.RemoveProjectile(playerProjectiles.at(j));
                     //playerProjectiles.erase(playerProjectiles.begin() + j);
                     enemies.at(i)->health--;
                     if (!(std::find(deleteProjectiles.begin(), deleteProjectiles.end(), j) != deleteProjectiles.end())) {
                         deleteProjectiles.push_back(j);
                     }
                 }
             }
             if (playerProjectiles.at(j)->GetPosition().second < -200) {
                 if (!(std::find(deleteProjectiles.begin(), deleteProjectiles.end(), j) != deleteProjectiles.end())) {
                     deleteProjectiles.push_back(j);
                 }
             }
         }
         for (int k = deleteProjectiles.size() - 1; k > -1; k--) {
             window.RemoveProjectile(playerProjectiles.at(deleteProjectiles.at(k)));
             delete playerProjectiles.at(deleteProjectiles.at(k));
             playerProjectiles.erase(playerProjectiles.begin() + deleteProjectiles.at(k));
         }
         deleteProjectiles.clear();
     }
     for (int k = deleteEnemies.size() - 1; k > -1; k--) {
         window.DestroyShip(enemies.at(deleteEnemies.at(k)));
         delete enemies.at(deleteEnemies.at(k));
         enemies.erase(enemies.begin() + deleteEnemies.at(k));
     }
     deleteEnemies.clear();

     if (player->health <= 0) {
         window.DestroyShip(player);
         window.GameOver();
     }

     for (Ship* item : enemies) {
          item->Move();
     }
     for (Projectile* item : playerProjectiles) {
          item->Move();
     }
     for (Projectile* item : enemyProjectiles) {
          item->Move();
     }
     for(Ship* item : enemies) {
             item->Shoot(window, this);
     }
     player->Move(dirX, dirY, isKeyboard);
     player->Shoot(window, this);
     score++;
}

void Game::SetHorizontalMove(int dir)
{
     dirX = dir;
     isKeyboard = true;
}

void Game::SetVerticalMove(int dir)
{
     dirY = dir;
     isKeyboard = true;
}

void Game::MoveToPoint(const std::pair<int, int>& position)
{
     isKeyboard = false;
     player->destination = position;
}

void Game::StopMoveToPoint()
{
     dirX = 0;
     dirY = 0;
     isKeyboard = true;
}

const Ship* Game::GetPlayer()
{
     return player;
}


