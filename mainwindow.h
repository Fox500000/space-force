#pragma once

#include <QtCore>
#include <QGLWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QTimerEvent>
#include <QApplication>
#include <QBitmap>
#include <QFontDatabase>
#include <QScreen>

#include <set>

#include "defines.h"
#include "interfaces.h"

namespace Graphics {

class GameRenderer : public QGLWidget, public IRenderer {
    Q_OBJECT

    enum class MenuType {
        MainMenu,
        UpgradeMenu,
        Battle,
        GameOver
    };

    enum class BackgoundState {
        Menu,
        StartingFight,
        Fight
    };

    struct BgStar {
        QColor Color;
        QPointF Position;
        float Velocity;
    };
public:
    GameRenderer(QWidget *parent = nullptr);
    ~GameRenderer();
    void AddShip(const IShip *ship) final;
    void AddProjectile(const IProjectile *projectile) final;
    void RemoveShip(const IShip *ship) final;
    void DestroyShip(const IShip *ship) final;
    void RemoveProjectile(const IProjectile *projectile) final;
    void ClearBattle() final;
    void SetGame(IGame *game);
    void GameOver() final;
private:
    void DrawBackground(QPainter &painter);
    void DrawShips(QPainter &painter);
    void DrawProjectiles(QPainter &painter);
    void DrawExplosions(QPainter &painter);
    void DrawMainMenu(QPainter &painter);
    void DrawUpgradeMenu(QPainter &painter);
    void DrawBattleMenu(QPainter &painter);
    void DrawGameOverMenu(QPainter &painter);
private:
    void GenerateBgStar(BgStar &star, bool anywhere);
    void SetMenuState(MenuType state);
private:
    void paintEvent(QPaintEvent *event) final;
    void mousePressEvent(QMouseEvent *event) final;
    void mouseReleaseEvent(QMouseEvent *event) final;
    void mouseMoveEvent(QMouseEvent *event) final;
    void keyPressEvent(QKeyEvent *event) final;
    void keyReleaseEvent(QKeyEvent *event) final;
    void timerEvent(QTimerEvent *event) final;
    void UpdateControls(int key, bool pressed);
private:
    void MainMenuPressEvent(int index, const QPoint &pos);
    void UpgradeMenuPressEvent(int index, const QPoint &pos);
    void BattleMenuPressEvent(const QPoint &pos);
    void GameOverMenuPressEvent(int index, const QPoint &pos);
private:
    IGame *Game;

    QSize Window = QSize(600, 800);
    float ScreenScale = 1;

    MenuType State = MenuType::MainMenu;
    std::vector <std::vector <QRect> > MenuData;

    std::vector <QPixmap> ShipData;
    std::vector <QPixmap> ProjectileData;
    int ExplosonFrames;
    int ExplosionDuration = 60;
    QPixmap ExplosionData;
    std::pair <QPixmap, QPixmap> HealthData;

    std::set <const IShip*> ShipList;
    std::set <const IProjectile*> ProjectileList;
    std::list <std::pair <std::pair <float, float>, int> > ExplosionList;

    std::vector <BgStar> BackgroundStarList;
    float BgColorProgress = 1;
    float BgColor = 0;
    float BgBoostProgress = 0;
    BackgoundState BgState = BackgoundState::Menu;

    int FpsTimerId;
    int RenderTimerID;
    int FpsValue = 0;
    int FpsCounter = 0;

    QPen MenuPen;

    std::map <UpgradeType, std::string> UpgradeLabels;

    const QString CreditSymbol = QString("◆");
    const QString GoldSymbol = QString("₵");

    std::map <int, bool> KeyMap;
};

};
