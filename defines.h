#pragma once

enum class UpgradeType {
    Health,
    WeaponNumber,
    FireRate,
    Accuracity,
    Speed,
    Count
};
