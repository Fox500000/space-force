#pragma once

#include <iostream>
#include <cmath>

#include "interfaces.h"

class FakeShip: public IShip {
public:
    int GetType() const { return 0; }
    std::pair<float, float> GetPosition() const { return std::make_pair(250, 500); }
    float GetAngle() const { return M_PI / 4;}
};

class FakeProjectile: public IProjectile {


    // IProjectile interface
public:
    int GetType() const {return  0;}
    std::pair<float, float> GetPosition() const { return std::make_pair(300, 400);}
};

class FakeGame: public IGame {
public:
    FakeGame(IRenderer &renderer):
        Renderer(renderer)
    {}
    int GetSelectedShip() const {return 0;}
    int GetCurrentUpgradeLevel(UpgradeType type) const { return type == UpgradeType::Health ? 5 : 1; }
    int GetMaxUpgradeLevel(UpgradeType type) const { return 5; }
    std::pair<int, int> GetMoney() const { return std::make_pair(100, 5); }
    bool IsShipUnlocked(int id) const {return id == 0;}
    bool UpgradeCurrentShip(UpgradeType type, bool credits) {
        std::cout << "UPGRADE " << (int)type << " " << credits << std::endl;
        return false;
    }
    bool SelectShip(int id) {
        std::cout << "SELECT " << id << std::endl;
        return false;
    }
    void StartBattle() {
        Renderer.AddShip(&Ship);
        Renderer.AddProjectile(&Projectile);
    }
    void StopBattle() {
        Renderer.ClearBattle();
    }
    bool Revive() { return false; }
    std::pair <int, bool> GetShipPrice(int id) const {
        if (id == 2) return std::make_pair(15, true);
        else return std::make_pair(100, false);
    }
    std::pair<int, int> GetUpgradeCost(UpgradeType type) const {return std::make_pair(100, 5);}
    int GetMaxScore() const {return 1000;}
    int GetMaxHealth() const {return 4;}
    int GetCurrentHealth() const {return 2;}
    int GetScore() const {return 3213;}
    int GetEarnedCredits() const {return 43;}
    int GetReviveCost() const {return 5;}

    void SetHorizontalMove(int dir) {}
    void SetVerticalMove(int dir) {}
    void MoveToPoint(const std::pair <int, int> &position) {}
    void Update() {}
private:
    IRenderer &Renderer;
    FakeShip Ship;
    FakeProjectile Projectile;
};
