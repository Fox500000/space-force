#include "enemypatterns.h"

#include "game.h"
#include <cmath>

bool wallSpawned = false;

void SpawnRandomPattern(Game &game, IRenderer *window)  {
    Ship *enemy;
    Projectile *projectile;
    float holePos;
    float x;

    int type = 0;
    int typeCount = 5;
    int typeChances[] = {10, 10, 10, 10, 10};
    int totalChance = 0;
    for (int i = 0; i < typeCount; i++) totalChance += typeChances[i];
    int chance = rand() % (totalChance);
    for (int i = 0; i < typeCount; i++) {
        chance -= typeChances[i];
        if (chance <= 0) {
            type = i;
            break;
        }
    }

    switch(type) {
    case 0:
        enemy = new Ship(true, std::make_pair(10 + rand() % (windowX - 10), -100), 3, 0);
        game.enemies.push_back(enemy);
        window->AddShip(enemy);
        break;
    case 1:
        x = 200 + rand() % (windowX - 400);
        for(int i = 0; i < 5; i++) {
            enemy = new Ship(true, std::make_pair(x, -100 * i), 3, 1);
            game.enemies.push_back(enemy);
            window->AddShip(enemy);
        }
        break;
    case 2:
        x = 200 + rand() % (windowX - 400);
        for(int i = 0; i < 5; i++) {
            enemy = new Ship(true, std::make_pair(x, -100 * i), 3, 2);
            game.enemies.push_back(enemy);
            window->AddShip(enemy);
        }
        break;
    case 3:
        enemy = new Ship(true, std::make_pair(rand() % windowX, -50), 3, 3);
        game.enemies.push_back(enemy);
        window->AddShip(enemy);
        break;
    case 4:
            if(wallSpawned) {
                wallSpawned = false;
                break;
            }
            wallSpawned = true;
            holePos = rand() % windowX;
            for(int i = 0; i < windowX + 15; i += 15) {
                if(abs(holePos - i) > 50) {
                    projectile = new Projectile(true, std::make_pair(i, -10), 90, 1);
                    game.enemyProjectiles.push_back(projectile);
                    window->AddProjectile(projectile);
                }
            }
        break;
    default: break;
    }
}
