#include "mainwindow.h"
#include <QApplication>
#include <ctime>
#include <cstdlib>

#include "game.h"

int main(int argc, char* argv[])
{
     srand(time(0));
     QApplication a(argc, argv);
     Graphics::GameRenderer w;
     Game game(w);
     //FakeGame game(w);
     w.SetGame(&game);

#ifdef ANDROID
    w.showFullScreen();
#else
     w.show();
#endif
     return a.exec();
}
