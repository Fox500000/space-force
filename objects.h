#pragma once
#include "interfaces.h"
#include <cmath>
#define RAD 0.0174532925199432958

class Game;

class GlobalShip {
public:
    static const int baseSpeed = 5;
    static const int baseShootingRate = 2;
    static const int baseGunSpread = 60; //radian
    static const int ShipPriceInCredits[3];
    bool bought;

    int startingHp; //1
    float startingSpeed;
    int startingGunCount;
    int startingShootingRate;
    int startingGunSpread;
    int extraLife;

    int maxHpUpgrade; //= upgrade cycles count
    int maxGunCountUpgrade;
    int maxShootingRateUpgrade;
    int maxGunSpreadUpgrade;
    int maxSpeedUpgrade;

    int currentHpUpgrade = 0; //= upgrade cycles count
    int currentGunCountUpgrade = 0;
    int currentShootingRateUpgrade = 0;
    int currentGunSpreadUpgrade = 0;
    int currentSpeedUpgrade = 0;

    //stats for battle
    int maxHp = 1;
    int gunCount = 1;
    int shootingRate = 1;
    int gunSpread = 60;
    float speed = 1;

    int _credits = 10;
    int gold = 5;

    GlobalShip();

    GlobalShip(int type);

    void Setup(int type);
    bool UpgradeShip(UpgradeType type, bool creditsSelected, int &credits, int &gold);
    std::pair<int,int> GetCost(UpgradeType type);
    void UpdateUpgrades();




};

// enemy 90

// friendly -90
class Ship: public IShip {
private:
    std::pair<float, float> position;
    float angle;
    int counter = 0;
    int fpsCounter = 0;
    bool moveLock;
public:
    int type;
    int moveType = 0;
    int health;
    int gunStat;
    int gunSpread;
    float rateOfFire;
    float speed;
    float boundingSphereRadius;
    std::pair<float, float> destination;

    Ship(bool isEnemy, std::pair<float, float> pos, int type, int moveType);

    int GetType() const;
    std::pair<float, float> GetPosition() const;
    float GetAngle() const;
    void Setup(GlobalShip* settings);
    void Shoot(IRenderer &renderer, Game* game);
    void Move(float x, float y, bool isKeyboard);
    void Move();
    void Move(std::pair<float, float> position);
    void SetPosition(float x, float y);
};

class Projectile: public IProjectile {
private:
    int type;
    std::pair<float, float> position;
    std::pair<float, float> delta;
public:
    float boundingSphereRadius;
    Projectile(bool isEnemy, std::pair<float, float> pos, float angle, float speed);
    int GetType() const;
    std::pair<float, float> GetPosition() const;
    void Move();
};

