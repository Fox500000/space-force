#pragma once

#include "defines.h"
#include <utility>

class IShip {
public:
    virtual ~IShip() = default;
    virtual int GetType() const  = 0;  //вернуть тип корабля
    virtual std::pair <float, float> GetPosition() const = 0; //вернуть позицию корабля (x,y)
    virtual float GetAngle() const = 0; //вернуть угол направления корабля в радианах
};

class IProjectile {
public:
    virtual ~IProjectile() = default;
    virtual int GetType() const = 0; //вернуть тип снаряда
    virtual std::pair <float, float> GetPosition() const = 0; //вернуть позицию снаряда (x, y)
};

class IGame {
public: //глобальные значение
    virtual ~IGame() = default;
    //вернуть id выбранного корабля в ангаре
    virtual int GetSelectedShip() const = 0;

    //вернуть стоимость следующего уровня улучшения (обычная цена, донатная цена) для текущего корабля
    //если это максимальный уровень то (0, 0)
    virtual std::pair<int, int> GetUpgradeCost(UpgradeType type) const = 0;

    //вернуть текущий уровень улучшения для данного типа улучшения для текущего корабля
    virtual int GetCurrentUpgradeLevel(UpgradeType type) const = 0;

    //вернуть максимальный уровень улучшеня для данного типа улучшения для текущего корабля
    virtual int GetMaxUpgradeLevel(UpgradeType type) const = 0;

    //вернуть деньги игрока (обычная влюта, донатная)
    //обычная валюта БЕЗ УЧЕТА ЗАРАБОТАННОЕ В ДАННОМ БОЮ(если игрок в бою)
    virtual std::pair<int, int> GetMoney() const = 0;

    //проверка разблокирован ли корабль данного типа
    virtual bool IsShipUnlocked(int id) const = 0;

    //вернуть цену кобля типа id, true если за донат, иначе false
    virtual std::pair <int, bool> GetShipPrice(int id) const = 0;

    //вернуть рекорд очков игрока
    virtual int GetMaxScore() const = 0;

    //вернуть цену воскрешения игрока(только за донат)
    virtual int GetReviveCost() const = 0;
public: //значения в бою
    //максимальное здоровье игрока в бою
    virtual int GetMaxHealth() const = 0;

    //текущие здоровье игрока в бою
    virtual int GetCurrentHealth() const = 0;

    //текущие очки  в бою
    virtual int GetScore() const = 0;

    //заработанная обычная валюта за текущий забег
    virtual int GetEarnedCredits() const = 0;
public: //глобальное управление
    //попытаться улучшить текущий корабль
    //type - вид улучшения
    //credits - улучшение за кредиты или за донат. true означает кредиты
    virtual bool UpgradeCurrentShip(UpgradeType type, bool credits) = 0;

    //выбрать корабль в ангаре если уже куплен
    //иначе попытаться купить
    virtual bool SelectShip(int id) = 0;

    //начать забег
    virtual void StartBattle() = 0;

    //закончить забег
    virtual void StopBattle() = 0;

    //воскресить корабль игрока в бою
    virtual bool Revive() = 0;

    //обновлении физики раз в фпс
    virtual void Update() = 0;
public: //управление в бою
    //задаёт направление горизонтального перемещения
    virtual void SetHorizontalMove(int dir) = 0;

    //задаёт направление вертикального перемещения
    virtual void SetVerticalMove(int dir) = 0;

    //перемещать корабль в эту точку(со скоростью корабля, а не телепортировать)
    virtual void MoveToPoint(const std::pair <int, int> &position) = 0;

    //отменить перемещение корабля в точку
    virtual void StopMoveToPoint() = 0;
};

class IRenderer {
public:
    virtual ~IRenderer() = default;

    //добавить корабль для отрисовки
    virtual void AddShip(const IShip *ship) = 0;

    //добавить снаряд для отрисовки
    virtual void AddProjectile(const IProjectile *projectile) = 0;

    //убрать корабль из отрисовки
    virtual void RemoveShip(const IShip *ship) = 0;

    //разрушить корабль, с эффектом взрыва
    //пока что тоже самое что и удаление
    virtual void DestroyShip(const IShip *ship) = 0;

    //убрать снаряд из отрисовки
    virtual void RemoveProjectile(const IProjectile *projectile) = 0;

    //убрать все объекты из отрисовки
    virtual void ClearBattle() = 0;

    //показать экран game over
    virtual void GameOver() = 0;
};
