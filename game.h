#pragma once

#include "objects.h"
#include <vector>
#include "collision.h"
#include <random>
#include "mainwindow.h"
#include "enemypatterns.h"

using namespace std;

#define windowX 600
#define windowY 800

class Game: public IGame {
  private:
     Ship* player;
     std::vector<GlobalShip*> playerUpgrades;
     int chosenType = 0;
    // GlobalShip* playerUpgrades;
     IRenderer& window;
     int fpsCounter = 0;
     float dirX = 0;
     float dirY = 0;
     ulong score = 0;
     ulong maxScore = 0;
     bool isKeyboard = false;
     int credits = 1000;
     int gold = 500;
     int earnedCredits = 0;
     int revivesCount = 0;
  public:
     vector<Projectile*> enemyProjectiles;
     vector<Projectile*> playerProjectiles;
     vector<Ship*> enemies;

     void ClearField();

     Game(IRenderer& windowIn);
     ~Game();

     // IGame interface
  public:
     int GetSelectedShip() const;
     std::pair<int, int> GetUpgradeCost(UpgradeType type) const;
     int GetCurrentUpgradeLevel(UpgradeType type) const;
     int GetMaxUpgradeLevel(UpgradeType type) const;
     std::pair<int, int> GetMoney() const;
     bool IsShipUnlocked(int id) const;
     std::pair<int, bool> GetShipPrice(int id) const;
     int GetMaxScore() const;
     int GetReviveCost() const;
     int GetMaxHealth() const;
     int GetCurrentHealth() const;
     int GetScore() const;
     int GetEarnedCredits() const;
     bool UpgradeCurrentShip(UpgradeType type, bool credits);
     bool SelectShip(int id);
     void StartBattle();
     void StopBattle();
     bool Revive();
     void Update();
     void SetHorizontalMove(int dir);
     void SetVerticalMove(int dir);
     void MoveToPoint(const std::pair<int, int>& position);
     void StopMoveToPoint() final;
     const Ship* GetPlayer();
};

