#include "objects.h"
#include "game.h"

Ship::Ship(bool isEnemy, std::pair<float, float> pos, int type, int moveType)
{
     boundingSphereRadius = 15.0f;
     if (isEnemy) {
          this->type = 3;
          angle = 90.0f;
     }
     else {
          this->type = type;
          angle = -90.0f;
     }
     position = pos;
     health = 5;
     gunStat = 1;
     gunSpread = 60;
     rateOfFire = 0.3;
     speed = 3;
     this->moveType = moveType;
     moveLock = false;

}

int Ship::GetType() const
{
     return type;
}

std::pair<float, float> Ship::GetPosition() const
{
     return position;
}

float Ship::GetAngle() const
{
     return angle * RAD;
}

void Ship::Setup(GlobalShip* settings)
{
     health = settings->maxHp;
     gunStat = settings->gunCount;
     gunSpread = settings->gunSpread;
     rateOfFire = settings->shootingRate;
     speed = settings->speed;
}

void Ship::Shoot(IRenderer& renderer, Game* game)
{
     float fireSpeed = (60 / (rateOfFire * 4));
     float x = position.first;
     float y = position.second - boundingSphereRadius;
     std::pair<float, float> projectilePos = std::make_pair(x, y);
     if (counter > fireSpeed) {
          Projectile* projectile;
          if (type != 3) {
               switch (gunStat) {
                    case 1:
                         projectile = new Projectile(false, projectilePos, angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);
                         break;
                    case 2:
                         projectile = new Projectile(false, std::make_pair(x - 10, y), angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, std::make_pair(x + 10, y), angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);
                         break;
                    case 3:
                         projectile = new Projectile(false, projectilePos, angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, projectilePos, angle + (gunSpread / 2), 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, projectilePos, angle - (gunSpread / 2), 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);
                         break;
                    case 4:
                         projectile = new Projectile(false, std::make_pair(x - 10, y), angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, std::make_pair(x + 10, y), angle, 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, projectilePos, angle + (gunSpread / 2), 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         projectile = new Projectile(false, projectilePos, angle - (gunSpread / 2), 10);
                         game->playerProjectiles
                         .push_back(projectile);
                         renderer.AddProjectile(projectile);

                         break;
                    default: break;
               }
          }
          else {
              if(moveType == 3) {
               projectile = new Projectile(true, position, angle, 2);
               game->enemyProjectiles.push_back(projectile);
               renderer.AddProjectile(projectile);
              }
          }
          counter = 0;
     }
     else {
          counter++;
     }
}

void Ship::Move(float x, float y, bool isKeyboard)
{
     if (isKeyboard) {
          if (x != 0 || y != 0) {
               position.first += (x / (sqrt(pow(x, 2) + pow(y, 2)))) * speed;
               position.second += (y / (sqrt(pow(x, 2) + pow(y, 2)))) * speed;
          }
     }
     else {
          if (!(destination.first == position.first && destination.second == position.second)) {
               std::pair<float, float> vec;
               vec.first = destination.first - position.first;
               vec.second = destination.second - position.second;
               float length = sqrt(pow(vec.first, 2) + pow(vec.second, 2));
               float angle = atan2(vec.second, vec.first);
               x = cos(angle);
               y = sin(angle);
               position.first += x * (speed > length ? length : speed);
               position.second += y * (speed > length ? length : speed);
          }
     }
     if (position.first < 0)
          position.first = 0;
     if (position.first > windowX)
          position.first = windowX;
     if (position.second < 0)
          position.second = 0;
     if (position.second > windowY)
          position.second = windowY;
}

void Ship::Move()
{
     switch (moveType) {
          case 0:
               position.second += speed;
               break;
          case 1:
               position.second += speed;
               position.first += sin(position.second / 200) * 1.2;
               angle = 90 + (1 / RAD) * sin(position.second / 200 + 3.14);
               break;
     case 2:
         position.second += speed;
         position.first += -sin(position.second / 200) * 1.2;
         angle = 90 + (1 / RAD) * -sin(position.second / 200 + 3.14);
         break;
          case 3:
               fpsCounter++;
               if(fpsCounter <= 600) {
                    Move(std::make_pair(10 + rand() % (windowX - 10), 50 + rand() % 20));
               } else {
                    moveType = 0;
               }
               break;
          default: break;
     }
}

void Ship::Move(std::pair<float, float> position)
{
    if (!moveLock) {
        destination = position;
        moveLock = true;
    }
    else {
    if (!(abs(destination.first - this->position.first) < 1 && abs(destination.second - this->position.second) < 1)) {
        std::pair<float, float> vec;
        vec.first = destination.first - this->position.first;
        vec.second = destination.second - this->position.second;
        float length = sqrt(pow(vec.first, 2) + pow(vec.second, 2));
        this->position.first += length == 0 ? 0 : (vec.first / length) * (speed > length ? length : speed);
        this->position.second += length == 0 ? 0 : (vec.second / length) * (speed > length ? length : speed);
    }
    else {
        moveLock = false;
    }
    }

}

void Ship::SetPosition(float x, float y) {
    position.first = x;
    position.second = y;
}

Projectile::Projectile(bool isEnemy, std::pair<float, float> pos, float angle, float speed)
{
     boundingSphereRadius = 5.0f;
     if (isEnemy) {
          type = 1;
     }
     else {
          type = 0;
     }
     position = pos;
     delta.first = cos(angle * RAD) * speed;
     delta.second = sin(angle * RAD) * speed;
}

int Projectile::GetType() const
{
     return type;
}

std::pair<float, float> Projectile::GetPosition() const
{
     return position;
}

void Projectile::Move()
{
     position.first += delta.first;
     position.second += delta.second;
}

const int GlobalShip::ShipPriceInCredits[3] = {100, 200, 70};
GlobalShip::GlobalShip(int type)
{
     switch (type) {
          case 0: //default ship
               startingHp = 1;
               startingSpeed = baseSpeed * 2;
               startingGunCount = 1;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 2;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 3;
               maxGunSpreadUpgrade = 1;
               extraLife = 0;
               maxSpeedUpgrade = 1;

               bought = true;

               maxHp = 1;
               gunCount = 1;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed * 2;

               break;

          case 1: //fast ship
               startingHp = 1;
               startingSpeed = baseSpeed * 3;
               startingGunCount = 2;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 0;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 5;
               maxGunSpreadUpgrade = 2;
               extraLife = 0;
               maxSpeedUpgrade = 2;

               bought = false;

               maxHp = 1;
               gunCount = 2;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed * 3;

               break;

          case 2: //heavy weapons guy
               startingHp = 2;
               startingSpeed = baseSpeed;
               startingGunCount = 2;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 3;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 4;
               maxGunSpreadUpgrade = 2;
               extraLife = 1;
               maxSpeedUpgrade = 1;

               bought = false;

               maxHp = 2;
               gunCount = 2;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed;

               break;

          case 3: //enemy
               startingHp = 2;
               startingSpeed = baseSpeed / 4;
               startingGunCount = 1;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 0;
               maxGunCountUpgrade = 0;
               maxShootingRateUpgrade = 0;
               maxGunSpreadUpgrade = 0;
               extraLife = 0;
               maxSpeedUpgrade = 0;

               bought = false;

               maxHp = 2;
               gunCount = 1;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed / 4;

               break;

          default:
               break;
     }
}

void GlobalShip::Setup(int type)
{

     switch (type) {
          case 0: //default ship
               startingHp = 1;
               startingSpeed = baseSpeed * 2;
               startingGunCount = 1;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 2;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 3;
               maxGunSpreadUpgrade = 1;
               extraLife = 0;
               maxSpeedUpgrade = 1;

               maxHp = 1;
               gunCount = 1;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed * 2;

               break;

          case 1: //fast ship
               startingHp = 1;
               startingSpeed = baseSpeed * 3;
               startingGunCount = 2;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 0;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 5;
               maxGunSpreadUpgrade = 2;
               extraLife = 0;
               maxSpeedUpgrade = 2;

               maxHp = 1;
               gunCount = 2;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed * 3;

               break;

          case 2: //heavy weapons guy
               startingHp = 2;
               startingSpeed = baseSpeed;
               startingGunCount = 2;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 3;
               maxGunCountUpgrade = 2;
               maxShootingRateUpgrade = 4;
               maxGunSpreadUpgrade = 2;
               extraLife = 1;
               maxSpeedUpgrade = 1;

               maxHp = 2;
               gunCount = 2;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed;

               break;

          case 3: //enemy
               startingHp = 2;
               startingSpeed = baseSpeed / 4;
               startingGunCount = 1;
               startingShootingRate = baseShootingRate;
               startingGunSpread = baseGunSpread;
               maxHpUpgrade = 0;
               maxGunCountUpgrade = 0;
               maxShootingRateUpgrade = 0;
               maxGunSpreadUpgrade = 0;
               extraLife = 0;

               maxHp = 2;
               gunCount = 1;
               shootingRate = baseShootingRate;
               gunSpread = baseGunSpread;
               speed = baseSpeed / 4;

               break;

          default:
               break;
     }
}

bool GlobalShip::UpgradeShip(UpgradeType type, bool creditsSelected, int &credits, int &gold)
{
    int creditsMult = 10;
    int goldMult = 5;
     switch (type) {
          case UpgradeType :: Health:
               if (currentHpUpgrade < maxHpUpgrade) {
                    if (creditsSelected) {
                         if (credits - ((currentHpUpgrade + 1) * creditsMult) >= 0) {
                              credits -= ((currentHpUpgrade + 1) * creditsMult);

                         }
                         else {
                              return false;
                         }
                    }
                    else {
                         if (gold - ((currentHpUpgrade + 1) * goldMult) >= 0) {
                              gold -= ((currentHpUpgrade + 1) * goldMult);

                         }
                         else {
                              return false;
                         }

                    }
                    currentHpUpgrade++;
                    maxHp = startingHp + (1 * currentHpUpgrade);
                    return true;
               }
               break;
          case UpgradeType :: WeaponNumber:
               if (currentGunCountUpgrade < maxGunCountUpgrade) {
                    if (creditsSelected) {
                         if (credits - ((currentGunCountUpgrade + 1) * creditsMult) >= 0) {
                              credits -= ((currentGunCountUpgrade + 1) * creditsMult);

                         }
                         else {
                              return false;
                         }
                    }
                    else {
                         if (gold - ((currentGunCountUpgrade + 1) * goldMult) >= 0) {
                              gold -= ((currentGunCountUpgrade + 1) * goldMult);

                         }
                         else {
                              return false;
                         }

                    }
                    currentGunCountUpgrade++;
                    gunCount = startingGunCount + (1 * currentGunCountUpgrade);
                    return true;
               }

               break;
          case UpgradeType :: FireRate:
               if (currentShootingRateUpgrade < maxShootingRateUpgrade) {
                    if (creditsSelected) {
                         if (credits - ((currentShootingRateUpgrade + 1) * creditsMult) >= 0) {
                              credits -= ((currentShootingRateUpgrade + 1) * creditsMult);

                         }
                         else {
                              return false;
                         }
                    }
                    else {
                         if (gold - ((currentShootingRateUpgrade + 1) * goldMult) >= 0) {
                              gold -= ((currentShootingRateUpgrade + 1) * goldMult);
                         }
                         else {
                              return false;
                         }

                    }
                    currentShootingRateUpgrade++;
                    shootingRate = startingShootingRate + (1 * currentShootingRateUpgrade);
                    return true;
               }

               break;
          case UpgradeType :: Accuracity:
               if (currentGunSpreadUpgrade < maxGunSpreadUpgrade) {
                    if (creditsSelected) {
                         if (credits - ((currentGunSpreadUpgrade + 1) * creditsMult) >= 0) {
                              credits -= ((currentGunSpreadUpgrade + 1) * creditsMult);
                         }
                         else {
                              return false;
                         }
                    }
                    else {
                         if (gold - ((currentGunSpreadUpgrade + 1) * goldMult) >= 0) {
                              gold -= ((currentGunSpreadUpgrade + 1) * goldMult);
                         }
                         else {
                              return false;
                         }

                    }
                    currentGunSpreadUpgrade++;
                    gunSpread = startingGunSpread / (currentGunSpreadUpgrade == 0 ? 1 : (2 * currentGunSpreadUpgrade));
                    return true;
               }

               break;
          case UpgradeType :: Speed:
               if (currentSpeedUpgrade < maxSpeedUpgrade) {
                    if (creditsSelected) {
                         if (credits - ((currentSpeedUpgrade + 1) * creditsMult) >= 0) {
                              credits -= ((currentSpeedUpgrade + 1) * creditsMult);
                         }
                         else {
                              return false;
                         }
                    }
                    else {
                         if (gold - ((currentSpeedUpgrade + 1) * goldMult) >= 0) {
                              gold -= ((currentSpeedUpgrade + 1) * goldMult);
                         }
                         else {
                              return false;
                         }

                    }
                    currentSpeedUpgrade++;
                    speed = startingSpeed + (1 * currentSpeedUpgrade);
                    return true;
               }


               break;
          default:
               throw std::invalid_argument("No such upgrade type");
     }
     throw std::invalid_argument("Upgrade method ignored switch");
}

std::pair<int, int> GlobalShip::GetCost(UpgradeType type)
{
    int goldMult = 5;
    int creditsMult = 10;
     switch (type) {
          case UpgradeType :: Health:
               if (currentHpUpgrade < maxHpUpgrade) {
                    return make_pair((currentHpUpgrade + 1) * creditsMult, (currentHpUpgrade + 1) * goldMult);
               }
               return make_pair(0, 0);

          case UpgradeType :: WeaponNumber:
               if (currentGunCountUpgrade < maxGunCountUpgrade) {
                    return make_pair((currentGunCountUpgrade + 1) * creditsMult, (currentGunCountUpgrade + 1) * goldMult);
               }
               return make_pair(0, 0);

          case UpgradeType :: FireRate:
               if (currentShootingRateUpgrade < maxShootingRateUpgrade) {
                    return make_pair((currentShootingRateUpgrade + 1) * creditsMult, (currentShootingRateUpgrade + 1) * goldMult);
               }
               return make_pair(0, 0);

          case UpgradeType :: Accuracity:
               if (currentGunSpreadUpgrade < maxGunSpreadUpgrade) {
                    return make_pair((currentGunSpreadUpgrade + 1) * creditsMult, (currentGunSpreadUpgrade + 1) * goldMult);
               }
               return make_pair(0, 0);

          case UpgradeType :: Speed:
               if (currentSpeedUpgrade < maxSpeedUpgrade) {
                    return make_pair((currentSpeedUpgrade + 1) * creditsMult, (currentSpeedUpgrade + 1) * goldMult);
               }
               return make_pair(0, 0);

          default:
               throw std::invalid_argument("No such upgrade type");

     }
}

void GlobalShip::UpdateUpgrades()
{
    maxHp = startingHp + (1 * currentHpUpgrade);
    gunCount = startingGunCount + (1 * currentGunCountUpgrade);
    shootingRate = startingShootingRate + (1 * currentShootingRateUpgrade);
    gunSpread = startingGunSpread / (currentGunSpreadUpgrade == 0 ? 1 : (2 * currentGunSpreadUpgrade));
    speed = startingSpeed + (1 * currentSpeedUpgrade);
}
