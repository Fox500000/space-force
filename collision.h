#pragma once
#include "objects.h"

template <typename T>
bool CheckCollision(Ship* ship, T* object)
{
     std::pair<float, float> shipPos = ship->GetPosition();
     std::pair<float, float> objectPos = object->GetPosition();
     float x = objectPos.first - shipPos.first;;
     float y = objectPos.second - shipPos.second;
     float length = sqrt(pow(x, 2) + pow(y,2));
     if(length < (ship->boundingSphereRadius + object->boundingSphereRadius)) return true;
     return false;
//     float shipBBDiam = ship->boundingBoxDiameter;
//     float objectBBDiam = object->boundingBoxDiameter;
//     float shipBBRight = shipPos.first + shipBBDiam;
//     float shipBBLeft = shipPos.first - shipBBDiam;
//     float shipBBTop = shipPos.second + shipBBDiam;
//     float shipBBBottom = shipPos.second - objectBBDiam;
//     float objectBBRight = objectPos.first + objectBBDiam;
//     float objectBBLeft = objectPos.first - objectBBDiam;
//     float objectBBTop = objectPos.second + objectBBDiam;
//     float objectBBBottom = objectPos.second - objectBBDiam;
//     if (shipBBTop >= 0 && objectBBTop >= 0) {
//          if (shipBBRight < objectBBLeft ||
//              shipBBLeft > objectBBRight ||
//              shipBBTop < objectBBBottom ||
//              shipBBBottom > objectBBTop) {
//               return false;
//          }
//          else {
//               return true;
//          }
//     }
//     return false;
}

